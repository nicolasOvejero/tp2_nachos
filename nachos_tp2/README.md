Fichiers modifiés
	 - threads/list.cc
   	 - threads/scheduler.cc
   	 - threads/thread.cc
   	 - threads/thread.h
   	 - userprog/systemcall.cc
   	 - userprog/systemcall.h

Modification pour list.cc
	     - Création de la fonction Insert()
	     - Insertion d'un élément en fonction de la priorité des autres
	     dans la liste

Modification de scheduler.cc
	     - Dans la fonction ReadyToRun() remplacement de
	     readyList.Append() par readyList.Insert()
	     - Finilisation de la fonction ThreadOrder()

Modification de thread.h
	     - Création de la variable privée priority
	     - Création des prototypes getter/setter pour la varible priority

Modification de thread.cc
	     - Création des fonctions getter/setter pour la varible priority
	     - Initialisation dans le constructeur de la variable priority à
	     20
	     - Dans setter vérification de la priorité (entre 0 et 40)

Modification de systemcall.h
	     - Création du prototype system_Nice()

Modification de systemcall.cc
	     - Création de la fonction system_Nice() (attribut la priorité à
	     un thread)
	     - Ajout de l'appel de la fonction nice au switch
